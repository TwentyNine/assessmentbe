﻿using System;
using NUnit.Framework;
using warehouse.Services;
using Moq;
using System.Collections.Generic;
using warehouse.Models;
using warehouse.Repository.Interfaces;

namespace warehouseTest.ServiceTest.Test
{
    [TestFixture]
    public class ProductServiceTest
    {
        private ProductService productService;

        Mock<IProductRepository> productRepositoryMock ;

        private List<Product> products;
        private List<Product> noProducts;

        
        [SetUp]
        public void Setup()
        {
            productRepositoryMock = new Mock<IProductRepository>();

            productService = new ProductService(productRepositoryMock.Object);

            products = new List<Product>();
            products.Add(new Product() { Id = "abcdefghijklmnopqrstuvwxyz", categoryId = "29", discontinued = "0", productId= "1234", productName="productTest", quantityPerUnit = "11", reorderLevel = "7", supplierId = "31"  });
            products.Add(new Product() { Id = "aaaaaaaaaaaaaaaaaaaaaaaaaa", categoryId = "11", discontinued = "0", productId= "4321", productName="productTest1", quantityPerUnit = "7", reorderLevel = "11", supplierId = "07"  });
            products.Add(new Product() { Id = "bbbbbbbbbbbbbbbbbbbbbbbbbb", categoryId = "29", discontinued = "0", productId= "5678", productName="productTest2", quantityPerUnit = "7", reorderLevel = "11", supplierId = "31"  });
            products.Add(new Product() { Id = "cccccccccccccccccccccccccc", categoryId = "29", discontinued = "0", productId= "8765", productName="productTest3", quantityPerUnit = "11", reorderLevel = "7", supplierId = "07"  });

            noProducts = new List<Product>();
        }

        [Test]
        public void TestShouldGetProducts()
        {
            productRepositoryMock.Setup(x => x.GetAll()).Returns(products);

            List<Product> productReturned = productService.Get();

            Assert.IsTrue(productReturned.Count == 4);
        }

        [Test]
        public void TestShouldNotGetProducts()
        {
            productRepositoryMock.Setup(x => x.GetAll()).Returns(noProducts);

            List<Product> productReturned = productService.Get();

            Assert.IsTrue(productReturned.Count == 0);
        }

        [Test]
        public void TestShouldGetProductsByCategoryID()
        {
            string search = "29";

            productRepositoryMock.Setup(x => x.GetAllProductByCategoryId(search)).Returns(products.FindAll(p => p.categoryId == search));

            List<Product> productReturned = productService.GetAllProductByCategoryId(search);

            Assert.IsTrue(productReturned.Count == 3);
        }

        [Test]
        public void TestShouldGetNoProductsIfCategoryIdDoesntExist()
        {
            string search = "29";

            productRepositoryMock.Setup(x => x.GetAllProductByCategoryId(search)).Returns(noProducts);

            List<Product> productReturned = productService.GetAllProductByCategoryId(search);

            Assert.IsTrue(productReturned.Count == 0);
        }

        [Test]
        public void TestShouldGetProductsBySupplierID()
        {
            string search = "31";
            productRepositoryMock.Setup(x => x.GetAllProductBySupplierId(search)).Returns(products.FindAll(p => p.supplierId == search));

            List<Product> productReturned = productService.GetAllProductBySupplierId(search);

            Assert.IsTrue(productReturned.Count == 2);
        }

        [Test]
        public void TestShouldGetNoProductsIfSupplierIdDoesntExist()
        {
            string search = "31";
            productRepositoryMock.Setup(x => x.GetAllProductBySupplierId(search)).Returns(noProducts);

            List<Product> productReturned = productService.GetAllProductBySupplierId(search);

            Assert.IsTrue(productReturned.Count == 0);
        }

        [Test]
        public void TestShouldGetNoProductsIfSupplierIdCouldntBeFound()
        {
            string search = "00";
            productRepositoryMock.Setup(x => x.GetAllProductBySupplierId(search)).Returns(products.FindAll(p => p.supplierId == search));

            List<Product> productReturned = productService.GetAllProductBySupplierId(search);

            Assert.IsTrue(productReturned.Count == 0);
        }
    }
}
