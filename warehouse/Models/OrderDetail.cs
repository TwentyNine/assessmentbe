﻿using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace warehouse.Models
{
    public class OrderDetail
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        public int orderId { get; set; }

        public int productId { get; set; }

        public double unitPrice { get; set; }

        public int quantity { get; set; }

        public double discount { get; set; }

    }
}
