﻿using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace warehouse.Models
{
    public class Supplier
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        public int supplierId { get; set; }

        public string companyName { get; set; }

        public string contactName { get; set; }

        public string contactTitle { get; set; }

        public string address { get; set; }

        public string city { get; set; }

        public string region { get; set; }

        public string postalCode { get; set; }

        public string country { get; set; }

        public string phone { get; set; }

        public string fax { get; set; }

        public string homepage { get; set; }

    }
}
