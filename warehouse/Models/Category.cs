﻿using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace warehouse.Models
{
    public class Category
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        [BsonElement("categoryId")]
        public int categoryId { get; set; }

        public string categoryName { get; set; }

        public string description { get; set; }

        public string picture { get; set; }

    }
}
