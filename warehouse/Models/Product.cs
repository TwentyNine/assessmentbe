﻿using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace warehouse.Models
{
    public class Product
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        public string productId { get; set; }

        public string productName { get; set; }

        public string supplierId { get; set; }

        public string categoryId { get; set; }

        public string quantityPerUnit { get; set; }

        public string unitPrice { get; set; }

        public string unitsInStock { get; set; }

        public string unitsInOrder { get; set; }

        public string reorderLevel { get; set; }

        public string discontinued { get; set; }

    }
}
