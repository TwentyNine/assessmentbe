﻿using System;
using MongoDB.Driver;
using warehouse.Models;
using System.Collections.Generic;
using System.Linq;
using warehouse.Repository.Interfaces;

namespace warehouse.Repository
{
    public class CategoryRepository: ICategoryRepository
    {
        private readonly IMongoCollection<Category> category;
        private List<Category> categories;

        public CategoryRepository(IWarehouseDatabaseSetting warehouseDatabaseSetting)
        {
            var client = new MongoClient(warehouseDatabaseSetting.ConnectionString);
            var database = client.GetDatabase(warehouseDatabaseSetting.DatabaseName);

            category = database.GetCollection<Category>(warehouseDatabaseSetting.CategoryCollectionName);
        }

        public List<Category> GetAll()
        {
            categories = category.Find(cat => true).ToList();
            return categories;
        } 
    }
}
