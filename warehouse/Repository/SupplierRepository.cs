﻿using System;
using MongoDB.Driver;
using warehouse.Models;
using System.Collections.Generic;
using System.Linq;
using warehouse.Repository.Interfaces;

namespace warehouse.Repository
{
    public class SupplierRepository : ISupplierRepository
    {
        private readonly IMongoCollection<Supplier> supplier;
        private List<Supplier> suppliers;

        public SupplierRepository(IWarehouseDatabaseSetting warehouseDatabaseSetting)
        {
            var client = new MongoClient(warehouseDatabaseSetting.ConnectionString);
            var database = client.GetDatabase(warehouseDatabaseSetting.DatabaseName);

            supplier = database.GetCollection<Supplier>(warehouseDatabaseSetting.SupplierCollectionName);
        }

        public List<Supplier> GetAll()
        {
            suppliers = supplier.Find(sup => true).ToList();
            return suppliers;
        }
    }
}
