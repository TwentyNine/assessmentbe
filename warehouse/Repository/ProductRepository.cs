﻿using System;
using MongoDB.Driver;
using warehouse.Models;
using System.Collections.Generic;
using System.Linq;
using warehouse.Repository.Interfaces;

namespace warehouse.Repository
{
    public class ProductRepository : IProductRepository
    {
        private readonly IMongoCollection<Product> product;
        private List<Product> products;

        public ProductRepository(IWarehouseDatabaseSetting warehouseDatabaseSetting)
        {
            var client = new MongoClient(warehouseDatabaseSetting.ConnectionString);
            var database = client.GetDatabase(warehouseDatabaseSetting.DatabaseName);

            product = database.GetCollection<Product>(warehouseDatabaseSetting.ProductCollectionName);
        }

        public List<Product> GetAll()
        {
            products = product.Find(product => true).ToList();
            return products;
        }

        public Product GetAll(string id) =>
            product.Find<Product>(product => product.Id == id).FirstOrDefault();

        public List<Product> GetAllProductByCategoryId(string categoryId) =>
            product.Find<Product>(product => product.categoryId == categoryId).ToList();
        

        public List<Product> GetAllProductBySupplierId(string supplierId) =>
            product.Find<Product>(product => product.supplierId == supplierId).ToList();


        public void Update(Product productToUpdate) =>
            product.ReplaceOne(product => product.productId == productToUpdate.productId, productToUpdate);

    }
}
