﻿using System;
using System.Collections.Generic;
using warehouse.Models;

namespace warehouse.Repository.Interfaces
{
    public interface ISupplierRepository
    {
        List<Supplier> GetAll();
    }
}
