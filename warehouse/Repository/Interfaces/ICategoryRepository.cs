﻿using System;
using System.Collections.Generic;
using warehouse.Models;

namespace warehouse.Repository.Interfaces
{
    public interface ICategoryRepository
    {
        List<Category> GetAll();
    }
}
