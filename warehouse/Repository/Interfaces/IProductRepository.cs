﻿using System;
using System.Collections.Generic;
using warehouse.Models;

namespace warehouse.Repository.Interfaces
{
    public interface IProductRepository
    {
        List<Product> GetAll();
        Product GetAll(string id);
        List<Product> GetAllProductByCategoryId(string categoryId);
        List<Product> GetAllProductBySupplierId(string supplierId);
        void Update(Product product);
    }
}
