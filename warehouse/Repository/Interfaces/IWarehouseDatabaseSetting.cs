﻿using System;
namespace warehouse.Repository.Interfaces
{
    public interface IWarehouseDatabaseSetting
    { 
        public string CategoryCollectionName { get; set; }
        public string ProductCollectionName { get; set; }
        public string SupplierCollectionName { get; set; }
        public string ConnectionString { get; set; }
        public string DatabaseName { get; set; }
    }
}
