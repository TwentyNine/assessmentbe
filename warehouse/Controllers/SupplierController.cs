﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using warehouse.Models;
using warehouse.Services;
using warehouse.Services.Interface;

namespace warehouse.Controllers
{
    [Route("api/v1/supplier/")]
    [ApiController]
    public class SupplierController : ControllerBase
    {
        private readonly ISupplierService supplierService;

        public SupplierController(ISupplierService supplierService)
        {
            this.supplierService = supplierService;
        }

        [HttpGet]
        public ActionResult<List<Supplier>> Get() =>
            supplierService.Get();

    }
}
