﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using warehouse.Models;
using warehouse.Services;
using warehouse.Services.Interface;

namespace warehouse.Controllers
{
    [Route("api/v1/category/")]
    [ApiController]
    public class CategoryController : ControllerBase
    {
        private readonly ICategoryService categoryService;

        public CategoryController(ICategoryService categoryService)
        {
            this.categoryService = categoryService;
        }

        [HttpGet]
        public ActionResult<List<Category>> Get() =>
            categoryService.Get();

    }
}
