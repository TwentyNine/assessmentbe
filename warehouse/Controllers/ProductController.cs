﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using warehouse.Models;
using warehouse.Services;
using warehouse.Services.Interface;

namespace warehouse.Controllers
{
    [Route("api/v1/product/")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private readonly IProductService productService;

        public ProductController(IProductService productService)
        {
            this.productService = productService;
        }

        [HttpGet]
        public ActionResult<List<Product>> Get() =>
            productService.Get();


        [HttpGet("category/{id}", Name = "GetProductByCategoryId")]
        public ActionResult<List<Product>> GetByCategoryId(string id)
        {
            var product = productService.GetAllProductByCategoryId(id);

            if (product == null)
            {
                return NotFound();
            }

            return product;
        }

        [HttpGet("supplier/{id}", Name = "GetProductBySupplierId")]
        public ActionResult<List<Product>> GetBySupplierId(string id)
        {
            var product = productService.GetAllProductBySupplierId(id);

            if (product == null)
            {
                return NotFound();
            }

            return product;
        }

        [HttpPut("update", Name = "UpdateProduct")]
        public ActionResult<bool> UpdateProduct([FromBody] Product product)
        {
            bool isUpdated = productService.UpdateProduct(product);

            return isUpdated;
        }
    }
}
