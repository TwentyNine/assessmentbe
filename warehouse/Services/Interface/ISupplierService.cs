﻿using System;
using System.Collections.Generic;
using warehouse.Models;

namespace warehouse.Services.Interface
{
    public interface ISupplierService
    {
        List<Supplier> Get();
    }
}
