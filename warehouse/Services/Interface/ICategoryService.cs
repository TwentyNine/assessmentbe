﻿using System;
using System.Collections.Generic;
using warehouse.Models;

namespace warehouse.Services.Interface
{
    public interface ICategoryService
    {
        List<Category> Get();
    }
}
