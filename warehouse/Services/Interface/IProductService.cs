﻿using System;
using System.Collections.Generic;
using warehouse.Models;

namespace warehouse.Services.Interface
{
    public interface IProductService
    {
        List<Product> Get();
        List<Product> GetAllProductByCategoryId(string categoryId);
        List<Product> GetAllProductBySupplierId(string supplierId);
        bool UpdateProduct(Product product);
    }
}
