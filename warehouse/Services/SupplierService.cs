﻿using System;
using System.Collections.Generic;
using warehouse.Models;
using warehouse.Repository.Interfaces;
using warehouse.Services.Interface;

namespace warehouse.Services
{
    public class SupplierService: ISupplierService
    {
        private List<Supplier> suppliers;
        private ISupplierRepository supplierRepository;

        public SupplierService(ISupplierRepository supplierRepository)
        {
            this.supplierRepository = supplierRepository;
        }

        public List<Supplier> Get()
        {
            suppliers = supplierRepository.GetAll();
            return suppliers;
        }
    }
}
