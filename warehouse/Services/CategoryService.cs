﻿using System;
using System.Collections.Generic;
using warehouse.Models;
using warehouse.Repository.Interfaces;
using warehouse.Services.Interface;

namespace warehouse.Services
{
    public class CategoryService: ICategoryService
    {
        private List<Category> categories;
        private ICategoryRepository categoryRepository;

        public CategoryService(ICategoryRepository categoryRepository)
        {
            this.categoryRepository = categoryRepository;
        }

        public List<Category> Get()
        {
            categories = categoryRepository.GetAll();
            return categories;
        }
    }
}
