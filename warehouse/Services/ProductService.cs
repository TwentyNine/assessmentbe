﻿using System;
using System.Collections.Generic;
using warehouse.Models;
using warehouse.Repository.Interfaces;
using warehouse.Services.Interface;

namespace warehouse.Services
{
    public class ProductService : IProductService
    {
        private List<Product> products;
        private IProductRepository productRepository;

        public ProductService(IProductRepository productRepository)
        {
            this.productRepository = productRepository;
        }

        public List<Product> Get()
        {
            products = productRepository.GetAll();
            return products;
        }

        public List<Product> GetAllProductByCategoryId(string categoryId)
        {
            products = productRepository.GetAllProductByCategoryId(categoryId);
            return products;
        }

        public List<Product> GetAllProductBySupplierId(string supplierId)
        {
            products = productRepository.GetAllProductBySupplierId(supplierId);
            return products;
        }

        public bool UpdateProduct(Product product)
        {
            try
            {
                 productRepository.Update(product);
                return true;
            } catch(Exception ex)
            {
                return false;
            }
        }
    }
}
