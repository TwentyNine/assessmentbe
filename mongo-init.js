db = db.getSiblingDB('warehouseDB');

db.createCollection('categories');
db.createCollection('products');
db.createCollection('orderDetail');
db.createCollection('suppliers');

db.categories.insertMany([
    { categoryId: '1', categoryName: 'Beverages', description: 'Soft drinks, coffees, teas, beers, and ales', picture: '' },
    { categoryId: '2', categoryName: 'Condiments', description: 'Sweet and savory sauces, relishes, spreads, and seasonings', picture: '' }
]);

db.orderDetail.insertMany([
    { orderId: '10255', productId: '2', unitPrice: '15.2', quantity: '20', discount: '0' },
    { orderId: '10258', productId: '2', unitPrice: '15.2', quantity: '50', discount: '0.2' },
    { orderId: '10258', productId: '5', unitPrice: '17', quantity: '65', discount: '0.2' },
    { orderId: '10262', productId: '5', unitPrice: '17', quantity: '12', discount: '0.2' },
    { orderId: '10285', productId: '1', unitPrice: '14.4', quantity: '45', discount: '0.2' },
    { orderId: '10289', productId: '3', unitPrice: '8', quantity: '30', discount: '0' }
]);


db.suppliers.insertMany([
    {
        supplierId: '1', companyName: 'Exotic Liquids', contactName: 'Charlotte Cooper',
        contactTitle: 'Purchasing Manager', address: '49 Gilbert St', city: 'London', region: '',
        postalCode: 'EC1 4SD', country: 'UK', phone: '(171)555-2222', fax: '', homepage: ''
    },
    {
        supplierId: '2', companyName: 'New Orleans Cajun Delights', contactName: 'Shelley Burke',
        contactTitle: 'Order Administrator', address: 'P.O. Box 78934', city: 'New Orleans', region: 'LA',
        postalCode: '70117', country: 'USA', phone: '(100)555-4822', fax: '', homepage: ''
    },
    {
        supplierId: '3', companyName: 'Grandma Kellys Homestead', contactName: 'Regina Murphy',
        contactTitle: 'Sales Representative', address: '707 Oxford Rd', city: 'Ann Arbor', region: 'MI',
        postalCode: '48104', country: 'USA', phone: '(313)555-5735', fax: '(313)555-3349', homepage: ''
    }
]);



db.products.insertMany([
    { productId: '1', productName: 'Chai', supplierId: '1', categoryId: '1', quantityPerUnit: '10 boxes x 20 bags', unitPrice: '18', unitsInStock: '39', unitsInOrder: '0', reorderLevel: '10', discontinued: '0' },
    { productId: '2', productName: 'Chang', supplierId: '1', categoryId: '1', quantityPerUnit: '24 - 12 oz bottles', unitPrice: '19', unitsInStock: '17', unitsInOrder: '40', reorderLevel: '25', discontinued: '0' },
    { productId: '3', productName: 'Aniseed Syrup', supplierId: '1', categoryId: '2', quantityPerUnit: '12 - 550 ml bottles', unitPrice: '10', unitsInStock: '13', unitsInOrder: '70', reorderLevel: '25', discontinued: '0' },
    { productId: '4', productName: 'Chef Antons Cajun Seasoning', supplierId: '2', categoryId: '2', quantityPerUnit: '48 - 6 oz jars', unitPrice: '22', unitsInStock: '53', unitsInOrder: '0', reorderLevel: '0', discontinued: '0' },
    { productId: '5', productName: 'Chef Antons Gumbo Mix', supplierId: '2', categoryId: '2', quantityPerUnit: '36 boxes', unitPrice: '21.35', unitsInStock: '0', unitsInOrder: '0', reorderLevel: '0', discontinued: '1' }
]);
